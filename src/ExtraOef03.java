/*
 * Lees een getal a in, bereken de absolute waarde en druk dit af.
 */

public class ExtraOef03 {

	public static void main(String[] args) {
		int getalA;
		getalA = Invoer.leesInt("Geef een getal in: ");
		if (getalA < 0) {
			getalA = getalA * -1;
			}
		System.out.println("De absolute waarde is: " + getalA);
	}

}