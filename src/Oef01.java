/*
 * Er worden 2 getallen ingelezen via het toetsenbord. Als de getallen gelijk zijn, wordt afgedrukt: "De twee getallen zijn gelijk".
 * In het ander geval wordt afgedrukt: "De twee getallen zijn ongelijk".
 */
public class Oef01 {

	public static void main(String[] args) {
	int getalA, getalB;
	getalA = Invoer.leesInt("Geef een getal in");
	getalB = Invoer.leesInt("Geef een getal in");
	if (getalA == getalB){
		System.out.println("De getallen zijn gelijk");
	}else {
		System.out.println("De getallen zijn niet gelijk");
		}
	}

}
