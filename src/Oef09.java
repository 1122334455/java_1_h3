/*
 * Invoer: Naam speler, prijs vorig seizoen, leeftijd, gemiddeld cijfer (0-10), type speler, aantal goals (doelman=geincasseerdede goals).
 * Basisprijs = prijs vorig seizoen, leeftijd<25  = +10%, leeftijd>30 =-5%.
 * Aanvallers: eerste 5 goals = 10000 per goal, vanaf 6 goals = 20000 per goal.
 * Andere spelers: 10.000 * beoordelingscijfer.
 * Doelmannen: vanaf 20 goals = -9000 euro
 * Druk af: naam, prijs vorig seizoen, nieuwe prijs.
 */

public class Oef09 {
 
	public static void main(String[] args) {
		String naamSp, typeSp;
		double prijsVs, beCijfer, prijsNieuw = 0;
		int leeftijd, aantDoelp;
		naamSp = Invoer.leesString("Geef de naam van de speler in: ");
		prijsVs = Invoer.leesDouble("Geef de prijs van vorig seizoen in: ");
		leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
		beCijfer = Invoer.leesDouble("Geef het beoordelingscijfer in: ");
		typeSp = Invoer
				.leesString("Geef het type speler in (Aanvaller, Middenvelder, Verdediger of Doelman): ");
		aantDoelp = Invoer.leesInt("Geef het aantal doelpunten in: ");
		if (leeftijd < 25) {
			prijsNieuw = prijsVs * 1.1;
		} else {
			if (leeftijd >= 30) {
				prijsNieuw = prijsVs * 0.95;
			}
		}
		switch (typeSp) {
		case "Aanvaller":
			if (aantDoelp <= 5) {
				prijsNieuw += aantDoelp * 10000;
			} else {
				prijsNieuw += 10000 * 5 + 20000 * (aantDoelp - 5);
			}
			break;
		case "Doelman":
			prijsNieuw += (beCijfer * 10000);
			if (aantDoelp >= 20) {
				prijsNieuw -= 9000;
			}
			break;
		case "Middenvelder":
		case "Verdediger":
			prijsNieuw += beCijfer * 10000;
		}
		System.out.println("Naam speler:" + naamSp + ", prijs vorig seizoen: "
				+ prijsVs + ", nieuwe prijs: " + prijsNieuw);
	}

}

// Bert, 100000, Aanvaller, 24 jaar, 7 doelpunten, 3 beoordelingscijfer -> 200000