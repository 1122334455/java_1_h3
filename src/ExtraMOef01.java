/*
 * Gegeven zijn 3 getallen, a, b en c. Voor deze alle 3 in en bereken het kwadraat van het grootste getal.
 * Geen samengestelde voorwaarden gebruiken -> if (getala>getalC && etc).
 */

public class ExtraMOef01 {

	public static void main(String[] args) {
		int getalA, getalB, getalC, kwadraat;
		getalA = Invoer.leesInt("Geef een getal in: ");
		getalB = Invoer.leesInt("Geef een getal in: ");
		getalC = Invoer.leesInt("Geef een getal in: ");
		if (getalA > getalB) {
			if (getalA > getalC) {
				kwadraat = getalA * getalA;
			} else {
				kwadraat = getalC * getalC;
			}
		} else if (getalB > getalC) {
			kwadraat = getalB * getalB;
		} else {
			kwadraat = getalC * getalC;
		}
		System.out.println("Het kwadraat van het grootste getal is: "
				+ kwadraat);
	}

}
