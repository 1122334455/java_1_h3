/*
 * Prijsberekening van een vakantie. Invoer:
 * sterren 1-5, code O (enkel ontbijt), H (half-pension), V (vol-pension), A (all-inclusive), aantal overnachtingen, seizoen H (hoog), L (laag), T (tussenseizoen).
 * De prijs wordt als volgt berekend: 1 ster = 30/nacht, 2-3 sterren = 40/nacht, 4-5 sterren = 55/nacht.
 * De maaltijdprijs wordt als volgt berekend: O = 20% van de overnachtingskost, H = 50%, V = 60%.
 * Bij all-inclusive betaal je altijd 80 euro/nacht, ongeacht het aantal sterren (maaltijden inbegrepen).
 * Er is een extra korting van 10% tijdens het laagseizoen voor codes O en H.
 * Geef de nodige gegevens in en druk de prijs van een vakantie voor 1 persoon af. 
 */
public class Oef08 {

	public static void main(String[] args) {
		int sterren, nachten;
		double prijs = 0;
		char code, seizoen;
		nachten = Invoer.leesInt("Geef het aantal nachten in: ");
		sterren = Invoer.leesInt("Geef de sterren in: ");
		code = Invoer.leesChar("Geef de code in: ");
		seizoen = Invoer.leesChar("Geef het seizoen in: ");
		if (code == 'A') {
			prijs = nachten * 80;
		} else {
			switch (sterren) {
			case 1:
				prijs = nachten * 30;
				break;
			case 2:
			case 3:
				prijs = nachten * 40;
				break;
			case 4:
			case 5:
				prijs = nachten * 55;
			default:
				System.out.println("Geef een correct aantal sterren in:");
			}
			switch (code) {
			case 'O':
				prijs *= 1.2;
				break;
			case 'H':
				prijs *= 1.5;
				break;
			case 'V':
				prijs *= 1.6;
				break;
			default:
				System.out.println("Geef een correcte code in: ");
			}
			if (seizoen == 'L' && (code == 'O' || code == 'H')) {
				prijs *= 0.9;
			}
		}
		System.out.println("De totale prijs is: " + prijs);
	}

}
/*
 * test:
 * 
 * nachten 2, sterren 3, code A, seizoen L = 160.0 nachten 1, sterren 1, code O,
 * seizoen L = 32.4 nachten 1, sterren 1, code H, seizoen L = 40.5
 */
