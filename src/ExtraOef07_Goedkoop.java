/*
 * Prijsberekening water: vast recht = 25 euro, meer dan 30 kubieke meter = meerverbruik.
 * > 30 en <=200 = 1 euro/kubieke meter, > 200 en <= 5000 = 1.15 euro/kubieke meter.
 * Als het totale verbruik groter dan 5000 kubieke meter is, betaal je 1.175 per kubieke meter. 
 */

public class ExtraOef07_Goedkoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int verbruik;
		double prijs;
		final double VASTRECHT = 25.0;
		verbruik = Invoer.leesInt("Geef het verbruik in: ");
		if (verbruik < 30) {
			prijs = VASTRECHT;
		}else if (verbruik <= 200) {
				prijs = VASTRECHT + ((verbruik - 30) * 1.000);
			}else if (verbruik <= 5000) {
				prijs = VASTRECHT + ((verbruik - 200) * 1.150) + 200;
			}else {
				prijs = VASTRECHT + ((verbruik - 5000) * 1.175) + (4800 * 1.150) + (200 * 1.000);
			}
		System.out.println("De totale prijs is: " + prijs);
		}
			
	}
