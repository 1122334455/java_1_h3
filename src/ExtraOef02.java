/*
 * Lees 2 getallen a en b in en code K. K = 1 -> a+b, anders a-b.
 * Druk de 2 getallen, de code en het resultaat af.
 */

public class ExtraOef02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int getalA, getalB, codeK, uitKomst;
		getalA = Invoer.leesInt("Geef een getal in: ");
		getalB = Invoer.leesInt("Geef een getal in: ");
		codeK = Invoer.leesInt("Geef een code in: ");
		if (codeK == 1) {
			uitKomst = getalA+getalB;
			}else {
				uitKomst = getalA-getalB;
			}
		System.out.println("Getal A: " + getalA + ", Getal B: " + getalB + ", code K: " + codeK + ", uitkomst: " + uitKomst);
	}

}
