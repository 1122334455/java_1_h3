/*
 * Er worden 3 getallen ingelezen, a, b en c. Als a+b kleiner is dan 20, tel er c bij op.
 * Als a en b samen groter of gelijk zijn aan 20, druk de tekst "te groot" af.
 */

public class ExtraOef04 {

	public static void main(String[] args) {
		int getalA, getalB, getalC, som = 0;
		getalA = Invoer.leesInt("Geef een getal in: ");
		getalB = Invoer.leesInt("Geef een getal in: ");
		getalC = Invoer.leesInt("Geef een getal in: ");
		if ((getalA + getalB) < 20) {
			som = getalA + getalB + getalC;
			System.out.println("De som is: " + som);
		}else{
			System.out.println("Te groot");
		}
	}

}
