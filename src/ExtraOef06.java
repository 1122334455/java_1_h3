/*
 * Druk een vliegticket af. Naam, bestemming, afstand in km, klasse, het vertrekuur en de vertreminuten worden ingevoerd.
 * Druk af: naam, bestemming, afstand, klasse en het tijdstip dat men aanwezig dient te zijn: 1u45 voor vertrek, maaltijd Ja/Nee.
 * Prijsberekening: <1000km = 0.25/km, 1000-2999km = 0.20/km, >2999km = 0.15/km.
 * Klasse : Toerist (T), Charter (C) = -20%, Zaken (Z) = +30%.
 * Voor vluchten vanaf 1000km is er een maaltijd voorzien.
 */
public class ExtraOef06 {

	public static void main(String[] args) {
		String naam, bestemming, maaltijd;
		int afstandKm, vertrekUur, vertrekMin;
		// int totAanwMin, totVertMin, totAanw = 0, aanwUur, aanwMin;
		char klasse;
		double ticketPrijs = 0, ticketPrijsN;
		naam = Invoer.leesString("Geef de naam in: ");
		bestemming = Invoer.leesString("Geef de bestemming in: ");
		afstandKm = Invoer.leesInt("Geef de afstand in kilometers in: ");
		klasse = Invoer.leesChar("Geef de klasse in (T, C of Z): ");
		vertrekUur = Invoer.leesInt("Geef het vertrek uur in: ");
		vertrekMin = Invoer.leesInt("Geef de vertrek minuten in: ");
		if (afstandKm < 1000) {
			ticketPrijsN = afstandKm * 0.25;
			maaltijd = "Nee";
		} else if (afstandKm < 3000) {
			ticketPrijsN = afstandKm * 0.20;
			maaltijd = "Ja";
		} else {
			ticketPrijsN = afstandKm * 0.15;
			maaltijd = "Ja";
		}
		switch (klasse) {
		case 'Z':
			ticketPrijs = ticketPrijsN * 1.3;
			break;
		case 'C':
			ticketPrijs = ticketPrijsN * 0.8;
			break;
		case 'T':
			ticketPrijs = ticketPrijsN;
		} // met een IF schrijven
		/*
		 * totVertMin = ((vertrekUur * 60) + vertrekMin); totAanwMin =
		 * ((vertrekUur * 60) + vertrekMin) - 105; if (totVertMin < 105) { if
		 * (totAanwMin < 0) { totAanw = 1440 + totAanwMin; }else { totAanw =
		 * 1440 - totAanwMin; } }else { totAanw = totAanwMin; } aanwUur =
		 * totAanw / 60; aanwMin = totAanw - (aanwUur * 60);
		 */
		if (vertrekMin >= 45) {
			vertrekUur -= 1;
			vertrekMin -= 45;
		} else {
			vertrekUur -= 2;
			vertrekMin += 15;
		}
		if (vertrekUur < 0) {
			vertrekUur += 24;
		}
		System.out.println("Naam: " + naam);
		System.out.println("Bestemming: " + bestemming);
		System.out.println("Afstand (km): " + afstandKm);
		System.out.println("Klasse: " + klasse);
		System.out.println("Normale ticket prijs: " + ticketPrijsN);
		System.out.println("Totale ticket prijs: " + ticketPrijs);
		// System.out.println("Gelieve ten laatste aanwezig te zijn op het volgende tijdstip: "
		// + aanwUur + ":" + aanwMin);
		System.out
				.println("Gelieve ten laatste aanwezig te zijn op het volgende tijdstip: "
						+ vertrekUur + ":" + vertrekMin);
		System.out.println("Maaltijd: " + maaltijd);
		;
	}

}
// min groter dan 45 -> u - 1 en m - 45
// min kleiner dan 45 -> u -2 en m + 15