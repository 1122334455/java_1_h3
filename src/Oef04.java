/*
 * Lees twee getallen in en deel het grootste door het kleinste. Opgepast met delen door nul.
 */

public class Oef04 {

	public static void main(String[] args) {
		int getal1, getal2;
		getal1 = Invoer.leesInt("Geef een getal in: ");
		getal2 = Invoer.leesInt("Geef een getal in: ");
		if (getal1 > getal2 && getal2 != 0) {
			System.out.println("De uitkomst is " + getal1 / getal2);
		} else if (getal1 <= getal2 && getal1 != 0) {
			System.out.println("De uitkomst is " + getal2 / getal1);
		} else {
			System.out.println("Geen deling door nul!");
		}
	}
}