/*
 * Geef via het toetsenbord het brutoloon van een werknemer in. Gevraagd wordt het jaarlijks vakantiegeld en de jaarlijkse bijdrage te berekenen en af te drukken.
 * Het vakantiegeld is 5% van dit brutoloon. Is dit vakantiegeld minstens 350 euro, dan is jaarlijkse bijdrage gelijk aan 8% van 350 euro.
 * Is dit vakantiegeld kleiner dan 350 euro, dan is de jaarlijkse bijdrage 8% van het vakantiegeld.
 * Druk voor iedere werknemer af: brutoloon, vakantiegeld en jaarlijkse bijdrage.
 * Als deze bedragen worden afgerond op 1 decimaal na de komma.
 */


public class Oef02 {

	public static void main(String[] args) {
			double brutoLoon, vakGeld, bijdrage;
			brutoLoon = (int) ((Invoer.leesDouble("Geef het brutoloon in: ")) * 10 + 0.5) / 10; //(double) Math.round (Brutoloon * 10) / 10;
			vakGeld = (int) ((brutoLoon * 0.05) * 10 + 0.5) / 10;
			if (vakGeld >= 350){
				bijdrage = (int) ((350 * 0.08) * 10 + 0.5) / 10;
			}else {
				bijdrage = (int) ((vakGeld * 0.08) * 10 + 0.5) / 10;
			}
			System.out.println("Brutoloon: " + brutoLoon + " Vakantiegeld: " + vakGeld + " Bijdrage: " + bijdrage);
			}

}