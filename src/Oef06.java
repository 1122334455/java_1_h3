/*
 * De telefoonmaatschappij rekent aan haar klanten een vast abonnementsgeld van 17 euro voor een grote zone, 14.5 voor een niddelgrote zone en 12 euro voor een kleine zone.
 * Bij deze bedragen komt nog 3 euro voor huur en onderhoud van het toestel. Per zonale of interzonale eenheid betaal u 15 eurocent, de prijzen zijn excl BTW.
 * Geef via het toetsenbord het aantal zone en interzonale eenheden in, alsook of u tot een grote, middelgrote of kleine zone behoort.
 * Het programma berekent het te betalen bedrag inclusief BTW, dit percentage bedraagt 21%.
 * Maak in je oplossing gebruik van een selectieblok. 
 */

public class Oef06 {

	public static void main(String[] args) {
		char zone;
		double prijsEenheid = 0.15, huurOnd = 3, prijsG = 17, prijsM = 14.5, prijsK = 12, prijs;
		int zoEenheid, izEenheid;
		final double btw = 0.21;
		zoEenheid = Invoer.leesInt("Geef een zonale eenheid in: ");
		izEenheid = Invoer.leesInt("Geef een interzonale eenheid in: ");
		zone = Invoer.leesChar("Geef een zone code in (G, M of K): ");
		prijs = ((zoEenheid + izEenheid) * prijsEenheid) + huurOnd;
		switch (zone) {
		case 'G':
			prijs = prijs + prijsG;
			break;
		case 'M':
			prijs = prijs + prijsM;
			break;
		case 'K':
			prijs = prijs + prijsK;
			break;
		default:
			System.out.println("Geef een correcte zone code in (G, M of K");
		}
		prijs = prijs + (prijs * btw);
		System.out.println("De totale prijs is: " + prijs);
	}

}
