/*
 * Schrijf een programma dat toelaat de leeftijd en aansluitingsjaar van een lid van een tennisclub in te voeren.
 * Op basis hiervan wordt zijn bedrage berekend en afgedrukt. Volgende normen zijn van toepassing:
 * Basisbedrag = 100 euro, reductie1 = 14.5 euro indien het lid nog geen 21 is of ouder dan 60.
 * redcuctie2 = 2.5 per aangesloten jaar. Het minimum bedrag is 62.5 euro.
 * Het huidige jaar wordt ingevoerd via het toetsenbord.
 */

public class Oef07 {

	public static void main(String[] args) {
		int leeftijd, aansluitJaar, huidigJaar;
		double reductieLeeftijd = 14.5, reductieAansl = 2.5, minBedrag = 62.5, basisBedrag = 100, prijs;
		leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
		aansluitJaar = Invoer.leesInt("Geef het aansluitingsjaar in: ");
		huidigJaar = Invoer.leesInt("Geef het huidige jaar in: ");
		reductieAansl = (huidigJaar - aansluitJaar) * reductieAansl;
		if (leeftijd < 21 || leeftijd > 60) {
			prijs = (basisBedrag - reductieLeeftijd) - reductieAansl;
		} else {
			prijs = basisBedrag - reductieAansl;
		}
		if (prijs < minBedrag) {
			prijs = minBedrag;
		}
		System.out.println("De totale prijs is: " + prijs);
	}
}
