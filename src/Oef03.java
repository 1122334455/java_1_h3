/*
 * Bereken de aankomsttijd van een vlucht, de vertrektijd (uren en minuten) en de duur in minuten worden ingevoerd.
 * Bv: vertrekuur = 22, vertrekminuut = 18, duur = 170 -> aankomstuur = 1, aankomstminuut = 8. 
 */

public class Oef03 {

	public static void main(String[] args) {
		int vertrekUur, vertrekMin, duurMin, aankomstUur, aankomstMin, duurUren;
		vertrekUur = Invoer.leesInt("Geef het vertrekuur in: ");
		vertrekMin = Invoer.leesInt("Geef het vertrekminuten in: ");
		duurMin = Invoer.leesInt("Geef de duur in minuten in: ");
		duurUren = (int) (vertrekMin + duurMin) / 60;
		aankomstUur = vertrekUur + duurUren;
		aankomstMin = (duurMin + vertrekMin) - (duurUren * 60);
		if (aankomstUur >= 24) {
			aankomstUur = aankomstUur - 24;
		}
		System.out.println("Het aankomstuur is: " + aankomstUur
				+ ", de aankomstminuut is: " + aankomstMin);
	}

}
